# Gitlab CICD pipelines

This project is for showing a demo of how `CICD pipeines` gets created and how `stages` get created in pipelines and running some `tasks` in each stage and finally `deploying` the website to some serverless environment.

```
This project is implemented using Gatsby framework for a quick start demo to generate some static content.
```

## `.gitlab-ci.yml`

this file describes the pipeline structure and defines the stages and the tasks to be performed.



`pipeline` is a process that will be having set of instructions that needs to be executed on Gitlab server. it is combination of multiple stages.
`stage` is a set of relavent instructions which needs to be executed one after the other. 

once we have updated this file with required stages, pipelines will gets created in Gitlab environment.

![](https://gitlab.com/einstein06/my-static-website/-/raw/master/src/images/pipelines.PNG)
